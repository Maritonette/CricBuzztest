package in.sish.cricbuzz.ui.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import in.sish.cricbuzz.R;


public class HomeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater inflater;
    private Context mContext;
    private static final int TYPE_SPLITTER = 0;
    private static final int TYPE_OTHER = 1;

    ArrayList<JSONObject> mMatchList;


    public HomeListAdapter(Context context, ArrayList<JSONObject> List) {
        this.mContext = context;
        this.mMatchList = List;

        inflater = LayoutInflater.from(context);
    }

    public void delete(int position) {
        notifyItemRemoved(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_OTHER) {
            View view = inflater.inflate(R.layout.row_item_match_list, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            return holder;
        } else
            return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MyViewHolder) {
            final MyViewHolder holderItems = (MyViewHolder) holder;

            holderItems.recyclerview.setHasFixedSize(true);

            // use a linear layout manager
            holderItems.recyclerview.setLayoutManager(new org.solovyev.android.views.llm.LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

            MatchesAdapter mAdapter = new MatchesAdapter(mContext, mMatchList);

            holderItems.recyclerview.setAdapter(mAdapter);
        }


    }

    @Override
    public int getItemCount() {

//        if (mAddressList != null && mAddressList.size() > 0) {
//
//
//            return mAddressList.size();
//        } else
            return 1;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView titleA;
//        TextView titleB;
//        TextView time;

        RecyclerView recyclerview;

        public MyViewHolder(View itemView) {
            super(itemView);

            recyclerview = (RecyclerView) itemView.findViewById(R.id.matchList);

//            titleA = (TextView) itemView.findViewById(R.id.txtTeamA);
//
//            titleB = (TextView) itemView.findViewById(R.id.txtTeamB);
//            time = (TextView) itemView.findViewById(R.id.txtTime);


        }
    }


    @Override
    public int getItemViewType(int position) {
//        if (position!=0) {
//
//            Date date= new Date(Long.parseLong(mMatchList.get(position).getString("startDate").));
//
//
//            return TYPE_SPLITTER;
//        }
        return TYPE_OTHER;

    }


}
