package in.sish.cricbuzz.ui;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import in.sish.cricbuzz.R;
import in.sish.cricbuzz.ui.adapters.HomeListAdapter;
import in.sish.cricbuzz.ui.adapters.MatchesAdapter;

public class HomeActivity extends AppCompatActivity {


    private RecyclerView mRecycler;

    private MatchesAdapter mAdapter;
    private Context mContext;
    ArrayList<JSONObject> array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mContext = this;

        mRecycler = (RecyclerView) findViewById(R.id.homeList);
        mRecycler.setHasFixedSize(true);

        // use a linear layout manager
        mRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        readJson task = new readJson();
        task.execute(new String[]{null});
    }


    private class readJson extends AsyncTask<String, Void, String> {
        JSONObject json;
        InputStream input;

        @Override
        protected String doInBackground(String... urls) {
            try {
                AssetManager assetManager = getAssets();
                input = assetManager.open("schedule.json");

                int size = input.available();
                byte[] buffer = new byte[size];
                input.read(buffer);
                input.close();

                String text = new String(buffer);
                json = new JSONObject(text);

                JSONArray jsonArray = new JSONArray(json.getString("list"));


                JSONObject jsonObject = jsonArray.getJSONObject(0);


                JSONArray jsonArrayMatches = new JSONArray(jsonObject.getString("list"));

                array = new ArrayList<JSONObject>();
                for (int i = 0; i < jsonArrayMatches.length(); i++) {
                    try {
                        array.add(jsonArrayMatches.getJSONObject(i));
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {


            mAdapter = new MatchesAdapter(mContext, array);

            mRecycler.setAdapter(mAdapter);


//            ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar1);
//            pb.setVisibility(View.INVISIBLE);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar1);
//            pb.setVisibility(View.VISIBLE);
//            data1.clear();
        }
    }


}
