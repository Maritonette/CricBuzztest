package in.sish.cricbuzz.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.sish.cricbuzz.R;


public class MatchesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater inflater;
    private Context mContext;
    private static final int TYPE_SPLITTER = 0;
    private static final int TYPE_OTHER = 1;

    ArrayList<JSONObject> mMatchList;


    public MatchesAdapter(Context context, ArrayList<JSONObject> addressList) {
        this.mContext = context;
        this.mMatchList = addressList;

        inflater = LayoutInflater.from(context);
    }

    public void delete(int position) {
        notifyItemRemoved(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_OTHER) {
            View view = inflater.inflate(R.layout.row_match, parent, false);
            MatchesAdapter.MyViewHolder holder = new MatchesAdapter.MyViewHolder(view);
            return holder;
        } else if (viewType == TYPE_SPLITTER) {
            View view = inflater.inflate(R.layout.row_item_timet, parent, false);
            MatchesAdapter.MyViewHolderSplitter holder = new MatchesAdapter.MyViewHolderSplitter(view);
            return holder;
        } else {
            return null;
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MyViewHolder) {
            final MyViewHolder holderItems = (MyViewHolder) holder;

            try {
                holderItems.titleA.setText(mMatchList.get(position).getString("teamAName"));
                holderItems.titleB.setText(mMatchList.get(position).getString("teamBName"));
                SimpleDateFormat formatter = new SimpleDateFormat("h:mm a");


                String asString = formatter.format(Long.parseLong(mMatchList.get(position).getString("startDate")) * 1000);

                holderItems.time.setText(asString);
                holderItems.matchCount.setText(mMatchList.get(position).getString("matchDesc"));
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if (holder instanceof MyViewHolderSplitter) {
            final MyViewHolderSplitter holderItems = (MyViewHolderSplitter) holder;

            try {
                holderItems.titleA.setText(mMatchList.get(position).getString("teamAName"));
                holderItems.titleB.setText(mMatchList.get(position).getString("teamBName"));
                SimpleDateFormat formatter = new SimpleDateFormat("h:mm a");

                SimpleDateFormat formatter2 = new SimpleDateFormat("EEE, MMM d");

                Log.e("Date " + position, formatter2.format(Long.parseLong(mMatchList.get(position).getString("startDate")) * 1000));

                String asString = formatter.format(Long.parseLong(mMatchList.get(position).getString("startDate")) * 1000);

                holderItems.time.setText(asString);
                holderItems.matchCount.setText(mMatchList.get(position).getString("matchDesc"));
                holderItems.date.setText(formatter2.format(Long.parseLong(mMatchList.get(position).getString("startDate")) * 1000));
                holderItems.matchType.setText(mMatchList.get(position).getString("seriesDesc"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    @Override
    public int getItemCount() {

        if (mMatchList != null && mMatchList.size() > 0) {


            return mMatchList.size();
        } else
            return 0;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView titleA;
        TextView titleB;
        TextView time;
        TextView matchCount;


        public MyViewHolder(View itemView) {
            super(itemView);
            titleA = (TextView) itemView.findViewById(R.id.txtTeamA);

            titleB = (TextView) itemView.findViewById(R.id.txtTeamB);
            time = (TextView) itemView.findViewById(R.id.txtTime);
            matchCount = (TextView) itemView.findViewById(R.id.matchCount);


        }
    }

    class MyViewHolderSplitter extends RecyclerView.ViewHolder {
        TextView date;
        TextView matchType;
        TextView titleA;
        TextView titleB;
        TextView time;
        TextView matchCount;

        public MyViewHolderSplitter(View itemView) {
            super(itemView);

            titleA = (TextView) itemView.findViewById(R.id.txtTeamA);

            titleB = (TextView) itemView.findViewById(R.id.txtTeamB);
            time = (TextView) itemView.findViewById(R.id.txtTime);
            matchCount = (TextView) itemView.findViewById(R.id.matchCount);

            date = (TextView) itemView.findViewById(R.id.textView);
            matchType = (TextView) itemView.findViewById(R.id.txtLeagueName);


        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position != 0) {

            try {
                Date date = new Date(Long.parseLong(mMatchList.get(position).getString("startDate")) * 1000);
                Date prevDate = new Date(Long.parseLong(mMatchList.get(position - 1).getString("startDate")) * 1000);
                Log.e("Dates", "Date" + date.toString() + "  prevDate" + prevDate.toString());
                Calendar cal1 = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                cal1.setTime(date);
                cal2.setTime(prevDate);
                boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                        cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);

                if (sameDay) {
                    Log.e("Date  ", "SAme");

                    return TYPE_OTHER;

                } else {
                    return TYPE_SPLITTER;

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return TYPE_OTHER;

        } else return TYPE_SPLITTER;


    }


}
